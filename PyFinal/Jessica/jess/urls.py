__author__ = 'GB'
#-*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from jess import views

urlpatterns = patterns('jess.views',
    url(r'^$', views.index, name='index'),
    url(r'^biographie/$', 'biographie', name='biographie'),
    url(r'^oeuvres/$', 'oeuvres', name='oeuvres'),
    url(r'^evenement/$', 'evenement', name='evenement'),
    url(r'^img/$', 'img', name='img'),
     url(r'^img1/$', 'img1', name='img1'),
     url(r'^img2/$', 'img2', name='img2'),
     url(r'^img3/$', 'img3', name='img3'),
     url(r'^img4/$', 'img4', name='img4'),
     url(r'^img5/$', 'img5', name='img5'),
     url(r'^img6/$', 'img6', name='img6'),
    url(r'^contact/$', 'contact', name='contact'),
    url(r'^validation/$', 'validation', name='validation'),
)
