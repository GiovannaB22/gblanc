#-*- coding: utf-8 -*-
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.core.urlresolvers import reverse
from jess.form import ContactForm



def index(request):
    return render(request, 'jess/index.html')

def biographie(request):
    return render(request, 'jess/biographie.html')

def oeuvres(request):
    return render(request, 'jess/oeuvres.html')

def evenement(request):
    return render(request, 'jess/evenement.html')

def img(request):
    return render(request, 'jess/img.html')

def img1(request):
    return render(request, 'jess/img1.html')


def img2(request):
    return render(request, 'jess/img2.html')


def img3(request):
    return render(request, 'jess/img3.html')


def img4(request):
    return render(request, 'jess/img4.html')


def img5(request):
    return render(request, 'jess/img5.html')


def img6(request):
    return render(request, 'jess/img6.html')

def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponse("Form validate")
            return HttpResponseRedirect('jess/validation.html')
        else:
             return HttpResponse("Form invalid")

    else:
        form =ContactForm()
    return render_to_response('jess/contact.html',{'form':form }, context_instance = RequestContext(request))


def validation(request):
    return render(request, 'jess/validation.html')




