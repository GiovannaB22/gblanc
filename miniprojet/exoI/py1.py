# -*- coding: utf-8 -*-
import string
# definition de la fonction
#qui fait l'action de changement du contenu de fichier
def act_words(action):
#ouverture du fichier 
    with open(action) as file:
        """
exercise 1
Write a program that reads a ﬁle, breaks each line into words,
strips white space and punctuation from the words,
and converts them to lowercase
"""
        """
f recoit le fichier a lire, f1 recoit la transformation des espaces du fichier f
en saut de lignes, puis f2 recoit le fichier f1 avec l'enlevement des espaces et
ponctuation et enfin newf: le nouveau fichier recoit le fichier f2 avec les mots
en minuscule.
"""
        f= file.read()
        f1 = f.replace(" ",'\n')
        f2 = f1.translate(string.maketrans("",""),string.punctuation)
        newf = f2.lower()
        print(newf)
        
