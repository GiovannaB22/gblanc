import random
"""
creation d'une fonction qui gere l'apparition aleatoire de la probabilite des elements
dans un histogramme
"""
def choose_from_hist(lst):
   #creation de l'histogramme
    hist={}
    for mts in lst:
        hist[mts]=hist.get(mts, 0) + 1
    #omega: c'est le cardinal de la liste soit le nombre d'elements que celle-ci
    #contient 
    
    omega = len(lst)
    list = []
    for key in hist:
        hist[key] = str(str(hist[key])+"/"+ str(omega) )
        list.append(key)
        #gestion des apparitions grace a la longueur du dictionnaire
    show_rand = random.randint(0, len(hist[key]))
    return(list[show_rand]+":"+str(hist[list[show_rand]]))


if __name__ == '__main__':
     lst=['moi','moi','moi','toi','il','vous','vous','vous','toi','elle','elle']
     hist=choose_from_hist(lst)
     print hist
     


